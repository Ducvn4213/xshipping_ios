struct GigTicket {
    var id : Int?
    var name : String?
    var size : String?
    var paymentMethod : String?
    var cod : String?
    var note: String?
    var weight : Int?
    var createDate : String?
    var pickDate : String?
    
    var receiverName : String?
    var receiverPhone : String?
    var receiverAddress : String?
    var receiverCity : String?
    var receiverDistricts : String?
    var receiverWard : String?
    
    var senderName : String?
    var senderPhone : String?
    var senderAddress : String?
    var senderBankName : String?
    var senderBankNumber : String?
    var senderBankBranch : String?
    
    init() {}
}

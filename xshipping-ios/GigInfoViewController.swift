import UIKit

class GigInfoViewController : UIViewController {
    
    var hostNavController : UINavigationController!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    
    @IBOutlet weak var mName: UITextField!
    @IBOutlet weak var mWeight: UITextField!
    @IBOutlet weak var mSizeButton: UIButton!
    @IBOutlet weak var mPaymentMethodButton: UIButton!
    @IBOutlet weak var mPickDateButton: UIButton!
    @IBOutlet weak var mCod: UITextField!
    @IBOutlet weak var mNote: UITextView!
    
    @IBOutlet weak var mPickDateBackground: UIView!
    @IBOutlet weak var mPickDateContainer: UIView!
    
    @IBOutlet weak var mPickDateChoseButton: UIButton!
    @IBOutlet weak var mPickDateDatePicker: UIDatePicker!
    
    private var size : String = ""
    private var paymentMethod : String = ""
    private var pickDate : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate.hostNavController = self.navigationController
        mService = appDelegate.getFSService()
        hostNavController = appDelegate.getHostNavController()
        
        configEvent()
        configUI()
        loadPreviousData()
    }
    
    func loadPreviousData() {
        let editTicketObject = mService?.getEditTicketObject()
        if (editTicketObject != nil) {
            mName.text = editTicketObject?.ticketName
            mWeight.text = editTicketObject?.ticketWeight
            mSizeButton.setTitle(editTicketObject?.ticketSize, for: .normal)
            size = (editTicketObject?.ticketSize)!
            mPaymentMethodButton.setTitle(editTicketObject?.ticketPaymentMethod, for: .normal)
            paymentMethod = (editTicketObject?.ticketPaymentMethod)!
            mPickDateButton.setTitle("Ngày lấy hàng: " + (editTicketObject?.ticketPickDate)!, for: .normal)
            pickDate = "Ngày lấy hàng: " + (editTicketObject?.ticketPickDate)!
            mCod.text = editTicketObject?.ticketCOD
            mNote.text = editTicketObject?.ticketNote
            
            return
        }
        
        let preData = mService?.getGigInfo()
        
        if (preData != nil) {
            mName.text = preData?.name
            mWeight.text = "\(preData?.weight ?? 0)"
            mSizeButton.setTitle(preData?.size, for: .normal)
            mPaymentMethodButton.setTitle(preData?.paymentMethod, for: .normal)
            mPickDateButton.setTitle(preData?.pickDate, for: .normal)
            mCod.text = preData?.cod
            mNote.text = preData?.note
            
            size = (preData?.size)!
            paymentMethod = (preData?.paymentMethod)!
            pickDate = (preData?.pickDate)!
        }
    }
    
    func configUI() {
        mPickDateContainer.layer.cornerRadius = 3
        mPickDateContainer.layer.masksToBounds = true
        
        mPickDateChoseButton.layer.cornerRadius = 3
        mPickDateChoseButton.layer.masksToBounds = true
        
        mPickDateBackground.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        mPickDateBackground.isHidden = true
    }
    
    func configEvent() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SendGigViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func onNext(_ sender: Any) {
        if (mName.text == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Tên đơn hàng không thể thiếu")
            return
        }
        
        if (mWeight.text == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Khối lượng đơn hàng không thể thiếu")
            return
        }
        
        if (size == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Thông tin kích thước đơn hàng không thể thiếu")
            return
        }
        
        if (paymentMethod == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Thông tin phương thức thanh toán không thể thiếu")
            return
        }
        
        if (pickDate == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Thông tin ngày lấy hàng không thể thiếu")
            return
        }
        
        if (mService?.getCurrentUser() == nil) {
            self.requestLogin()
            return
        }
        
        mService?.setGigInfo(name: mName.text, weight: Int(mWeight.text!), size: size, payment: paymentMethod, cod: mCod.text, note: mNote.text, pickDate: pickDate)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "sender")
        self.navigationController?.pushViewController(someVC, animated: true)
    }
    
    func requestLogin() {
        let alert = UIAlertController(title: "Thông Báo", message: "Bạn phải đăng nhập để gửi đơn hàng. Bạn có muốn đăng nhập ngay không?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Có", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let someVC = storyBoard.instantiateViewController(withIdentifier: "login")
            self.hostNavController.viewControllers = [someVC]
        }))
        alert.addAction(UIAlertAction(title: "Bỏ qua", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func requestChangeSize(_ sender: Any) {
        let alert = UIAlertController(title: "Chọn kích thước", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "Nhỏ", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.mSizeButton.setTitle("Nhỏ", for: .normal)
            self.size = "Nhỏ"
        }))
        alert.addAction(UIAlertAction(title: "Trung bình", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.mSizeButton.setTitle("Trung bình", for: .normal)
            self.size = "Trung bình"
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func requestChangePaymentMethod(_ sender: Any) {
        let alert = UIAlertController(title: "Chọn phương thức thanh toán", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "Người giao trả", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.mPaymentMethodButton.setTitle("Người giao trả", for: .normal)
            self.paymentMethod = "Người giao trả"
        }))
        alert.addAction(UIAlertAction(title: "Người nhận trả", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.mPaymentMethodButton.setTitle("Người nhận trả", for: .normal)
            self.paymentMethod = "Người nhận trả"
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func requestChangePickDate(_ sender: Any) {
        self.mPickDateBackground.alpha = 0
        mPickDateBackground.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.mPickDateBackground.alpha = 1
        })
    }
    
    @IBAction func confirmPickDate(_ sender: Any) {
        if (verifyPickDate() == true) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let dateString = dateFormatter.string(from: mPickDateDatePicker.date)
            mPickDateButton.setTitle("Ngày lấy hàng: " + dateString, for: .normal)
            pickDate = "Ngày lấy hàng: " + dateString
            
            UIView.animate(withDuration: 0.3, animations: {
                self.mPickDateBackground.alpha = 0
            }, completion: {(Bool) in
                self.mPickDateBackground.isHidden = true
            })
        }
    }
    
    func verifyPickDate() -> Bool {
        var pickDate = NSCalendar.current.date(byAdding: .hour, value: 7, to: mPickDateDatePicker.date)
        var curDate = NSCalendar.current.date(byAdding: .hour, value: 7, to: Date())
        
        pickDate = NSCalendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: pickDate!)
        curDate = NSCalendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: curDate!)
        
        if (pickDate! < curDate!) {
            Utils.showDialog(host: self, title: "Tìm Shipper", message: "Bạn không thể chọn ngày trong quá khứ")
            return false
        }
        
        if (pickDate == curDate) {
            pickDate = mPickDateDatePicker.date
            let hours = NSCalendar.current.component(.hour, from: pickDate!)
            if (hours >= 16) {
                Utils.showDialog(host: self, title: "Tìm Shipper", message: "Hiện tại đã sau 16h, hãy chọn ngày mai hoặc những ngày khác vì chúng tôi không thể nhận đơn hàng sau 16h")
                return false
            }
        }
        
        return true
    }
}

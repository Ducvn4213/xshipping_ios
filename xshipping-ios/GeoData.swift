struct GeoData {
    var id: String?
    var name: String?
    
    init() {}
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}

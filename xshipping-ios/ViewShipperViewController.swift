import UIKit
import Cosmos

protocol GetShipperCallback {
    func onSuccess(data: Shipper)
    func onError()
}

class ViewShipperViewController : UIViewController {
    
    @IBOutlet weak var mAvatar: UIImageView!
    @IBOutlet weak var mName: UILabel!
    @IBOutlet weak var mTotalRating: UILabel!
    @IBOutlet weak var mRate: CosmosView!
    @IBOutlet weak var mEmail: UILabel!
    @IBOutlet weak var mPhone: UILabel!
    @IBOutlet weak var mCallButton: UIButton!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    
    var shipperID : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mService = appDelegate.getFSService()
        
        mService?.getShipperInfo(id: shipperID!, callback: self)
        
        correctUI()
    }
    
    func correctUI() {
        mCallButton.layer.cornerRadius = 5
        mCallButton.layer.masksToBounds = true
    }
    
    @IBAction func onCall(_ sender: Any) {
        if let url = URL(string: "telprompt://" + mPhone.text!)  {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}

extension ViewShipperViewController : GetShipperCallback {
    func onSuccess(data: Shipper) {
        mName.text = data.name
        mEmail.text = data.email
        mPhone.text = data.phone
        mTotalRating.text = data.totalRating! + " Ratings"
        mRate.rating = Double(data.rate!)!
        Utils.loadImageFor(container: mAvatar, link: data.avatar!)
    }
    
    func onError() {
        Utils.showDialog(host: self, title: "Tìm Shipper", message: "Có lỗi, vui lòng thử lại")
        self.navigationController?.popViewController(animated: true)
    }
}

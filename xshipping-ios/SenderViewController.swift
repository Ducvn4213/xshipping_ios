import UIKit
import GooglePlaces
import CoreLocation
import GooglePlacePicker

protocol GetDistanceAndCostCallback {
    func onSuccess(data: DistanceAndCost)
    func onError(error: String)
}

protocol CreateTicketCallback {
    func onSuccess()
    func onCreateError(error: String)
}

protocol EditTicketCallback {
    func onEditSuccess()
    func onEditError(error: String)
}

class SenderViewController : UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    var hostNavController : UINavigationController!
    var placesClient: GMSPlacesClient!
    let manager = CLLocationManager()
    var isEditingMode: Bool = false
    
    @IBOutlet weak var mName: UITextField!
    @IBOutlet weak var mPhone: UITextField!
    @IBOutlet weak var mAddress: UITextView!
    @IBOutlet weak var mSubmitButton: UIButton!
    @IBOutlet weak var mAddressInputWay: UIButton!
    @IBOutlet weak var mBankName: UITextField!
    @IBOutlet weak var mBankNumber: UITextField!
    @IBOutlet weak var mBankBranch: UITextField!
    @IBOutlet weak var mBankInfoContainer: UIView!
    @IBOutlet weak var mLoadingContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate.hostNavController = self.navigationController
        mService = appDelegate.getFSService()
        hostNavController = appDelegate.getHostNavController()
        placesClient = GMSPlacesClient.shared()
        
        configEvent()
        configUI()
        loadPreviousData()
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            manager.requestAlwaysAuthorization()
        }
    }
    
    @IBAction func requestAddressInputWay(_ sender: Any) {
        let alert = UIAlertController(title: "Chọn phương thức nhập địa chỉ", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "Sử dụng địa chỉ đăng ký", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.mAddress.text = self.mService?.getCurrentUser()?.address
            self.mAddressInputWay.setTitle("Sử dụng địa chỉ đăng ký", for: .normal)
        }))
        alert.addAction(UIAlertAction(title: "Sử dụng địa chỉ của bạn", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.loadCurrentLocation()
            self.mAddressInputWay.setTitle("Sử dụng địa chỉ của bạn", for: .normal)
        }))
        alert.addAction(UIAlertAction(title: "Chọn địa chỉ khác", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.requestPlacePicker()
            self.mAddressInputWay.setTitle("Chọn địa chỉ khác", for: .normal)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func requestPlacePicker() {
        let center = CLLocationCoordinate2D(latitude: 37.788204, longitude: -122.411937)
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePicker(config: config)
        
        placePicker.pickPlace(callback: {(place, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let place = place {
                //self.nameLabel.text = place.name
                self.mAddress.text = place.formattedAddress?.components(separatedBy: ", ")
                    .joined(separator: ", ")
            } else {
                Utils.showDialog(host: self, title: "Có Lỗi", message: "Không thể lấy được thông vị trí bạn chọn")
            }
        })
    }
    
    func loadCurrentLocation() {
        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let placeLikelihoodList = placeLikelihoodList {
                let place = placeLikelihoodList.likelihoods.first?.place
                if let place = place {
                    //self.mAddress.text = place.name
                    self.mAddress.text = place.formattedAddress?.components(separatedBy: ", ")
                        .joined(separator: ", ")
                }
                else {
                    Utils.showDialog(host: self, title: "Có Lỗi", message: "Không thể lấy được vị trí hiện tại của bạn")
                }
            }
        })
    }

    func loadPreviousData() {
        let editTicketObject = mService?.getEditTicketObject()
        if (editTicketObject != nil) {
            isEditingMode = true
            mName.text = editTicketObject?.senderName
            mPhone.text = editTicketObject?.senderPhone
            mAddress.text = editTicketObject?.senderAdress
            mBankName.text = editTicketObject?.senderBankName
            mBankNumber.text = editTicketObject?.senderBankNumber
            mBankBranch.text = editTicketObject?.senderBankBranch
            
            return
        }
        
        let user = mService?.getCurrentUser()

        if (user != nil) {
            mName.text = user?.name
            mPhone.text = user?.phone
            mAddress.text = user?.address
            mBankName.text = user?.bankName
            mBankNumber.text = user?.bankNumber
            mBankBranch.text = user?.bankBranch
        }
    }
    
    func configUI() {
        mSubmitButton.layer.cornerRadius = 3
        mSubmitButton.layer.masksToBounds = true
        
        let gigdata = mService?.getGigInfo()
        if (gigdata?.cod == "") {
            mBankInfoContainer.isHidden = true
        }
        else {
            mBankInfoContainer.isHidden = false
        }
        
        mLoadingContainer.isHidden = true
        mLoadingContainer.backgroundColor = UIColor.black.withAlphaComponent(0.8)
    }
    
    func configEvent() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SendGigViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        if (mName.text == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Tên người gửi không được trống")
            return
        }
        
        if (mPhone.text == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Số điện thoại người gửi không được trống")
            return
        }
        
        if (mAddress.text == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Địa chỉ người gửi không được trống")
            return
        }
        
        let gigdata = mService?.getGigInfo()
        if (gigdata?.cod != "") {
            if (mBankName.text == "") {
                Utils.showDialog(host: self, title: "Có Lỗi", message: "Tên tài khoản người gửi không được trống")
                return
            }
            
            if (mBankNumber.text == "") {
                Utils.showDialog(host: self, title: "Có Lỗi", message: "Số tài khoản người gửi không được trống")
                return
            }
            
            if (mBankBranch.text == "") {
                Utils.showDialog(host: self, title: "Có Lỗi", message: "Tên ngân hàng người gửi không được trống")
                return
            }
        }
        
        let rw = gigdata?.receiverWard
        let rd = gigdata?.receiverDistricts
        let rc = gigdata?.receiverCity
        
        let to = (gigdata?.receiverAddress)! + ", " + rw! + ", " + rd! + ", " + rc!
        let isBig = gigdata?.size == "Nhỏ" ? "false" : "true"
        mLoadingContainer.alpha = 0
        mLoadingContainer.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.mLoadingContainer.alpha = 1
        }, completion: {(Bool) in
            self.mService?.getDistanceAndCost(from: self.mAddress.text, to: to, isBig: isBig, callback: self)
        })
    }
    
    func doPostTicket() {
        mService?.setSender(name: mName.text, phone: mPhone.text, address: mAddress.text, bankName: mBankName.text, bankNumber: mBankNumber.text, bankBranch: mBankBranch.text)
        mLoadingContainer.alpha = 0
        mLoadingContainer.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.mLoadingContainer.alpha = 1
        }, completion: {(Bool) in
            if (self.isEditingMode) {
                self.mService?.editTicket(callback: self)
            }
            else {
                self.mService?.createTicket(callback: self)
            }
        })
    }
}

extension SenderViewController : GetDistanceAndCostCallback {
    func onSuccess(data: DistanceAndCost) {
        mLoadingContainer.alpha = 0
        mLoadingContainer.isHidden = true
        
        let du = data.distance! % 1000 > 0 ? 1 : 0
        let reDistance = data.distance!/1000 + du
        
        if (data.distance! > 50000) {
            Utils.showDialog(host: self, title: "Thông Báo", message: "Chúng tôi hiện chỉ hô trợ đơn hàng dưới 50km")
            return;
        }
        
        let alert = UIAlertController(title: "Tìm Shipper", message: "Quãng đường gửi: " + String(reDistance) + "km, chi phí: " + data.cost! + " VNĐ, bạn có đồng ý tiếp tục?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Có", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.doPostTicket()
        }))
        alert.addAction(UIAlertAction(title: "Không", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func onError(error: String) {
        mLoadingContainer.alpha = 0
        mLoadingContainer.isHidden = true
        Utils.showDialog(host: self, title: "Có Lỗi", message: "Chúng tôi không thể xác định được địa chỉ gửi hoặc nhận, hãy thử với địa chỉ khác")
    }
}

extension SenderViewController : CreateTicketCallback {
    func onSuccess() {
        mLoadingContainer.alpha = 0
        mLoadingContainer.isHidden = true
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "waitbook")
        self.hostNavController.viewControllers = [someVC]
        
        Utils.showDialog(host: self.hostNavController, title: "Tìm Shipper", message: "Tạo đơn hàng thành công")
    }
    
    func onCreateError(error: String) {
        mLoadingContainer.alpha = 0
        mLoadingContainer.isHidden = true
        
        Utils.showDialog(host: self, title: "Có Lỗi", message: "Tạo đơn hàng không thành công, xin vui lòng thử lại")
    }
}

extension SenderViewController : EditTicketCallback {
    func onEditSuccess() {
        mLoadingContainer.alpha = 0
        mLoadingContainer.isHidden = true
        mLoadingContainer.alpha = 0
        mLoadingContainer.isHidden = true
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "waitbook")
        self.hostNavController.viewControllers = [someVC]
        Utils.showDialog(host: self.hostNavController, title: "Tìm Shipper", message: "Chỉnh sửa đơn hàng thành công")
    }
    
    func onEditError(error: String) {
        mLoadingContainer.alpha = 0
        mLoadingContainer.isHidden = true
        
        Utils.showDialog(host: self, title: "Có Lỗi", message: "Chỉnh sửa đơn hàng không thành công, xin vui lòng thử lại")
    }
}

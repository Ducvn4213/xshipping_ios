import UIKit

class BasicTicketWithRedLabelCell : UITableViewCell {
    @IBOutlet weak var mName: UILabel!
    @IBOutlet weak var mSize: UILabel!
    @IBOutlet weak var mFrom: UILabel!
    @IBOutlet weak var mTo: UILabel!
    @IBOutlet weak var mRedLabel: UILabel!
}

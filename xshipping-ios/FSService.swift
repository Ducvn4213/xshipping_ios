import UIKit
import FacebookCore
import FacebookLogin


class FSService : NSObject {
    
    private var mCurrentUser : User?
    private var mCurrentGigTicket : GigTicket?
    private var mNetwork : Network?
    private var mEditingTicketObject: ResponseGigTicketDetail?
    
    override init() {
        super.init()
        
        mCurrentGigTicket = GigTicket()
        mNetwork = Network()
        
        checkLoginUser()
    }
    
    func getEditTicketObject() -> ResponseGigTicketDetail? {
        return self.mEditingTicketObject
    }
    
    func setEditingTicketObject(data: ResponseGigTicketDetail?) {
        self.mEditingTicketObject = data
    }
    
    func checkLoginUser() {
        if (checkFacebookAccount()) {
            return
        }
        
        let userString = getString(key: Constants.SHARE_PREFERENCE_USER_KEY)
        
        if (userString != nil) {
            do {
                let dataEncode = userString?.data(using: String.Encoding.utf8)
                let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : String]
                
                var user = User()
                user.id = dataJson["id"]
                user.accountID = dataJson["account"]
                user.address = dataJson["address"]
                user.bankBranch = dataJson["bank_branch"]
                user.bankName = dataJson["bank_name"]
                user.bankNumber = dataJson["bank_number"]
                user.email = dataJson["email"]
                user.isActive = dataJson["is_active"]
                user.name = dataJson["name"]
                user.phone = dataJson["phone"]
                user.token = dataJson["token"]
                user.avatar = dataJson["avatar"]
                
                setCurrentUser(user: user)
            }
            catch {}
        }
    }
    
    func checkFacebookAccount() -> Bool {
        if let act = AccessToken.current {
            let connection = GraphRequestConnection()
            connection.add(MyProfileRequest()) { response, result in
                switch result {
                case .success(let response):
                    let id = response.id
                    let name = response.name
                    let email = response.email
                    
                    self.loginFace(id: id!, name: name!, email: email!, callback: self)
                case .failed(let error):
                    print("Custom Graph Request Failed: \(error)")
                }
            }
            connection.start()
            return true
        }
        else {
            return false
        }
    }
    
    func verifySuccessAccount() {
        mCurrentUser?.isActive = "1"
        
        let userString = getString(key: Constants.SHARE_PREFERENCE_USER_KEY)
        let afterUserString = userString?.replacingOccurrences(of: "\"is_active\":\"0\"", with: "\"is_active\":\"1\"")
        self.saveString(string: afterUserString!, key: Constants.SHARE_PREFERENCE_USER_KEY)
    }
    
    func saveString(string: String, key: String) {
        let preferences = UserDefaults.standard
        preferences.set(string, forKey: key)
        preferences.synchronize()
    }
    
    func getString(key: String) -> String? {
        let preferences = UserDefaults.standard
        
        if preferences.string(forKey: key) == nil {
            return nil
        } else {
            return preferences.string(forKey: key)
        }
    }
    
    func setCurrentUser(user: User?) {
        self.mCurrentUser = user
    }
    
    func getCurrentUser() -> User? {
        return mCurrentUser
    }
    
    func clearCurrentGigTicket() {
        setReceiver(name: nil, phone: nil, address: nil, city: nil, districts: nil, ward: nil)
        setGigInfo(name: nil, weight: nil, size: nil, payment: nil, cod: nil, note: nil, pickDate: nil)
        setSender(name: nil, phone: nil, address: nil, bankName: nil, bankNumber: nil, bankBranch: nil)
    }
    
    func setReceiver(name: String?, phone: String?, address: String?, city: String?, districts: String?, ward: String?) {
        mCurrentGigTicket?.receiverName = name
        mCurrentGigTicket?.receiverPhone = phone
        mCurrentGigTicket?.receiverAddress = address
        mCurrentGigTicket?.receiverCity = city
        mCurrentGigTicket?.receiverDistricts = districts
        mCurrentGigTicket?.receiverWard = ward
    }
    
    func setGigInfo(name: String?, weight: Int?, size: String?, payment: String?, cod: String?, note: String?, pickDate: String?) {
        mCurrentGigTicket?.name = name
        mCurrentGigTicket?.weight = weight
        mCurrentGigTicket?.size = size
        mCurrentGigTicket?.paymentMethod = payment
        mCurrentGigTicket?.cod = cod
        mCurrentGigTicket?.note = note
        mCurrentGigTicket?.pickDate = pickDate
    }
    
    func getGigInfo() -> GigTicket? {
        if (mCurrentGigTicket?.name == nil) {
            return nil
        }
        
        return mCurrentGigTicket
    }
    
    func setSender(name: String?, phone: String?, address: String?, bankName: String?, bankNumber: String?, bankBranch: String?) {
        mCurrentGigTicket?.senderName = name
        mCurrentGigTicket?.senderPhone = phone
        mCurrentGigTicket?.senderAddress = address
        mCurrentGigTicket?.senderBankName = bankName
        mCurrentGigTicket?.senderBankNumber = bankNumber
        mCurrentGigTicket?.senderBankBranch = bankBranch
    }
    
    func getSender() -> GigTicket? {
        if (mCurrentGigTicket?.senderName == nil) {
            return nil
        }
        
        return mCurrentGigTicket
    }
    
    func loadDistricts(city_id: String, districtCallback: LoadDistrictCallback) {
        var params : [Param] = []
        params.append(Param(key: "_id", value: city_id))
        params.append(Param(key: "_request", value: "getdistricts"))
        
        mNetwork?.onComplete = {
            data in
                do {
                    let dataEncode = data.data(using: String.Encoding.utf8)
                    let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject
                    
                    let geoData = dataJson["data"] as AnyObject as? [AnyObject]
                    
                    var returnValue : [GeoData] = []
                    for geo in geoData! {
                        let gd = geo as! [String : String]
                        let gdi = GeoData(id: gd["districtid"]!, name: gd["name"]!)
                        
                        returnValue.append(gdi)
                    }
                    
                    districtCallback.onLoadDistrictSucess(data: returnValue)
                }
                catch {
                    districtCallback.onLoadDistrictError(error: "data invalid")
                }
            
        }
        
        mNetwork?.onError = {
            error in
                districtCallback.onLoadDistrictError(error: error)
            
        }
        
        mNetwork?.execute(link: Constants.HOST_GEO_DATA, params: params)
    }
    
    func loadWards(district_id: String, wardCallback: LoadWardCallback) {
        var params : [Param] = []
        params.append(Param(key: "_id", value: district_id))
        params.append(Param(key: "_request", value: "getwards"))
        
        mNetwork?.onComplete = {
            data in
            do {
                let dataEncode = data.data(using: String.Encoding.utf8)
                let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject
                
                let geoData = dataJson["data"] as AnyObject as? [AnyObject]
                
                var returnValue : [GeoData] = []
                for geo in geoData! {
                    let gd = geo as! [String : String]
                    let gdi = GeoData(id: gd["districtid"]!, name: gd["name"]!)
                    
                    returnValue.append(gdi)
                }
                
                wardCallback.onLoadWardSucess(data: returnValue)
            }
            catch {
                wardCallback.onLoadWardError(error: "data invalid")
            }
            
        }
        
        mNetwork?.onError = {
            error in
                wardCallback.onLoadWardError(error: error)
            
        }
        
        mNetwork?.execute(link: Constants.HOST_GEO_DATA, params: params)
    }
    
    func login(email: String, pass: String, callback : LoginCallback) {
        var params : [Param] = []
        params.append(Param(key: "_username", value: email))
        params.append(Param(key: "_password", value: pass))
        params.append(Param(key: "_request", value: "login"))
        
        mNetwork?.onComplete = {
            data in
            do {
                let dataEncode = data.data(using: String.Encoding.utf8)
                let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : String]
                
                var user = User()
                user.id = dataJson["id"]
                user.accountID = dataJson["account"]
                user.address = dataJson["address"]
                user.bankBranch = dataJson["bank_branch"]
                user.bankName = dataJson["bank_name"]
                user.bankNumber = dataJson["bank_number"]
                user.email = dataJson["email"]
                user.isActive = dataJson["is_active"]
                user.name = dataJson["name"]
                user.phone = dataJson["phone"]
                user.token = dataJson["token"]
                user.avatar = dataJson["avatar"]
                
                self.saveString(string: data, key: Constants.SHARE_PREFERENCE_USER_KEY)
                callback.onSuccess(user: user)
            }
            catch {
                callback.onError(error: "data invalid")
            }
        }
        
        mNetwork?.onError = {
            error in
            callback.onError(error: error)
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func loginFace(id: String, name: String, email: String, callback: LoginCallback) {
        var params : [Param] = []
        params.append(Param(key: "_faceid", value: id))
        params.append(Param(key: "_request", value: "loginface"))
        
        mNetwork?.onComplete = {
            data in
            do {
                let dataEncode = data.data(using: String.Encoding.utf8)
                let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : String]
                
                var user = User()
                user.id = dataJson["id"]
                user.accountID = dataJson["account"]
                user.address = dataJson["address"]
                user.bankBranch = dataJson["bank_branch"]
                user.bankName = dataJson["bank_name"]
                user.bankNumber = dataJson["bank_number"]
                user.email = email
                user.isActive = dataJson["is_active"]
                user.name = name
                user.phone = dataJson["phone"]
                user.token = dataJson["token"]
                user.avatar = dataJson["avatar"]
                
                self.saveString(string: "", key: Constants.SHARE_PREFERENCE_USER_KEY)
                callback.onSuccess(user: user)
            }
            catch {
                callback.onError(error: "data invalid")
            }
        }
        
        mNetwork?.onError = {
            error in
            callback.onError(error: error)
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func register(email: String, name: String, address: String, pass: String, callback: RegisterCallback) {
        var params : [Param] = []
        params.append(Param(key: "_email", value: email))
        params.append(Param(key: "_name", value: name))
        params.append(Param(key: "_address", value: address))
        params.append(Param(key: "_password", value: pass))
        params.append(Param(key: "_request", value: "register"))
        
        mNetwork?.onComplete = {
            data in
            do {
                let dataEncode = data.data(using: String.Encoding.utf8)
                let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : String]
                
                var user = User()
                user.id = dataJson["id"]
                user.accountID = dataJson["account"]
                user.address = dataJson["address"]
                user.bankBranch = dataJson["bank_branch"]
                user.bankName = dataJson["bank_name"]
                user.bankNumber = dataJson["bank_number"]
                user.email = dataJson["email"]
                user.isActive = dataJson["is_active"]
                user.name = dataJson["name"]
                user.phone = dataJson["phone"]
                user.token = dataJson["token"]
                user.avatar = dataJson["avatar"]
                
                self.saveString(string: data, key: Constants.SHARE_PREFERENCE_USER_KEY)
                callback.onSuccess(user: user)
            }
            catch {
                callback.onError(error: "data invalid")
            }
        }
        
        mNetwork?.onError = {
            error in
            callback.onError(error: error)
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func verify(phone: String, callback: VerifyPhoneNumberCallback) {
        var params : [Param] = []
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_phone", value: phone))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        params.append(Param(key: "_request", value: "verify"))
        
        mNetwork?.onComplete = {
            data in
            callback.onSucess()
        }
        
        mNetwork?.onError = {
            error in
            callback.onError()
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func changePassword(old: String, new: String, callback: ChangePasswordCallback) {
        var params : [Param] = []
        params.append(Param(key: "_oldpassword", value: old))
        params.append(Param(key: "_newpassword", value: new))
        params.append(Param(key: "_request", value: "change_password"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            callback.onSuccess()
        }
        
        mNetwork?.onError = {
            error in
            callback.onError()
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func getDistanceAndCost(from: String, to: String, isBig: String, callback: GetDistanceAndCostCallback) {
        var params : [Param] = []
        params.append(Param(key: "_from", value: from))
        params.append(Param(key: "_to", value: to))
        params.append(Param(key: "_is_big", value: isBig))
        params.append(Param(key: "_request", value: "get_distance_and_cost"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            do {
                let dataEncode = data.data(using: String.Encoding.utf8)
                let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : AnyObject]
                
                var returnValue = DistanceAndCost()
                returnValue.distance = dataJson["distance"] as? Int
                returnValue.cost = dataJson["cost"] as? String
                
                callback.onSuccess(data: returnValue)
            }
            catch {
                callback.onError(error: "data invalid")
            }
            
        }
        
        mNetwork?.onError = {
            error in
            callback.onError(error: error)
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func createTicket(callback: CreateTicketCallback) {
        var params : [Param] = []
        params.append(Param(key: "_name", value: (mCurrentGigTicket?.receiverName)!))
        params.append(Param(key: "_phone", value: (mCurrentGigTicket?.receiverPhone)!))
        params.append(Param(key: "_address", value: (mCurrentGigTicket?.receiverAddress)!))
        params.append(Param(key: "_city", value: (mCurrentGigTicket?.receiverCity)!))
        params.append(Param(key: "_district", value: (mCurrentGigTicket?.receiverDistricts)!))
        params.append(Param(key: "_ward", value: (mCurrentGigTicket?.receiverWard)!))
        params.append(Param(key: "_sender_address", value: (mCurrentGigTicket?.senderAddress)!))
        params.append(Param(key: "_gig_name", value: (mCurrentGigTicket?.name)!))
        params.append(Param(key: "_weight", value: "\(mCurrentGigTicket?.weight ?? 0)"))
        params.append(Param(key: "_payment_method", value: (mCurrentGigTicket?.paymentMethod)!))
        params.append(Param(key: "_size", value: (mCurrentGigTicket?.size)!))
        params.append(Param(key: "_date", value: (mCurrentGigTicket?.pickDate)!))
        params.append(Param(key: "_cod", value: (mCurrentGigTicket?.cod)!))
        params.append(Param(key: "_note", value: (mCurrentGigTicket?.note)!))
        params.append(Param(key: "_bank_name", value: (mCurrentGigTicket?.senderBankName)!))
        params.append(Param(key: "_bank_number", value: (mCurrentGigTicket?.senderBankNumber)!))
        params.append(Param(key: "_bank_bank", value: (mCurrentGigTicket?.senderBankBranch)!))
        params.append(Param(key: "_request", value: "create_gig_ticket"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            callback.onSuccess()
        }
        
        mNetwork?.onError = {
            error in
            callback.onCreateError(error: error)
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)

    }
    
    func editTicket(callback: EditTicketCallback) {
        var params : [Param] = []
        params.append(Param(key: "_name", value: (mCurrentGigTicket?.receiverName)!))
        params.append(Param(key: "_phone", value: (mCurrentGigTicket?.receiverPhone)!))
        params.append(Param(key: "_address", value: (mCurrentGigTicket?.receiverAddress)!))
        params.append(Param(key: "_city", value: (mCurrentGigTicket?.receiverCity)!))
        params.append(Param(key: "_district", value: (mCurrentGigTicket?.receiverDistricts)!))
        params.append(Param(key: "_ward", value: (mCurrentGigTicket?.receiverWard)!))
        params.append(Param(key: "_sender_address", value: (mCurrentGigTicket?.senderAddress)!))
        params.append(Param(key: "_gig_name", value: (mCurrentGigTicket?.name)!))
        params.append(Param(key: "_weight", value: "\(mCurrentGigTicket?.weight ?? 0)"))
        params.append(Param(key: "_payment_method", value: (mCurrentGigTicket?.paymentMethod)!))
        params.append(Param(key: "_size", value: (mCurrentGigTicket?.size)!))
        params.append(Param(key: "_date", value: (mCurrentGigTicket?.pickDate)!))
        params.append(Param(key: "_cod", value: (mCurrentGigTicket?.cod)!))
        params.append(Param(key: "_note", value: (mCurrentGigTicket?.note)!))
        params.append(Param(key: "_bank_name", value: (mCurrentGigTicket?.senderBankName)!))
        params.append(Param(key: "_bank_number", value: (mCurrentGigTicket?.senderBankNumber)!))
        params.append(Param(key: "_bank_bank", value: (mCurrentGigTicket?.senderBankBranch)!))
        params.append(Param(key: "_gig_ticket_id", value: (mEditingTicketObject?.id)!))
        params.append(Param(key: "_request", value: "update_gig_ticket"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            callback.onEditSuccess()
        }
        
        mNetwork?.onError = {
            error in
            callback.onEditError(error: error)
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func updateProfile(name: String, phone: String, address: String, bname: String, bnumber: String, bbranch: String, callback: UpdateProfileCallback) {
        var params : [Param] = []
        params.append(Param(key: "_name", value: name))
        params.append(Param(key: "_phone", value: phone))
        params.append(Param(key: "_address", value: address))
        params.append(Param(key: "_bank_name", value: bname))
        params.append(Param(key: "_bank_number", value: bnumber))
        params.append(Param(key: "_bank_branch", value: bbranch))
        params.append(Param(key: "_request", value: "update_profile"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            do {
                let dataEncode = data.data(using: String.Encoding.utf8)
                let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : String]
                
                var user = User()
                user.id = dataJson["id"]
                user.accountID = dataJson["account"]
                user.address = dataJson["address"]
                user.bankBranch = dataJson["bank_branch"]
                user.bankName = dataJson["bank_name"]
                user.bankNumber = dataJson["bank_number"]
                user.email = dataJson["email"]
                user.isActive = dataJson["is_active"]
                user.name = dataJson["name"]
                user.phone = dataJson["phone"]
                user.token = dataJson["token"]
                user.avatar = dataJson["avatar"]
                
                self.saveString(string: data, key: Constants.SHARE_PREFERENCE_USER_KEY)
                callback.onSuccess(data: user)
            }
            catch {
                callback.onError()
            }
        }
        
        mNetwork?.onError = {
            error in
            callback.onError()
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func updateAvatar(stringEncode: String, callback: UpdateAvatarCallback) {
        var params : [Param] = []
        params.append(Param(key: "_image", value: stringEncode))
        params.append(Param(key: "_request", value: "upload_image"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            callback.onSuccess()
        }
        
        mNetwork?.onError = {
            error in
            callback.onAvatarError()
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func getWaitBookTicket(callback: BasicTicketCallback) {
        getGigTicket(id: "1", callback: callback)
    }
    
    func getWaitPickTicket(callback: BasicTicketCallback) {
        getGigTicket(id: "2", callback: callback)
    }
    
    func getErrorTicket(callback: BasicTicketCallback) {
        getGigTicket(id: "6", callback: callback)
    }
    
    func getWaitCODTicket(callback: BasicTicketCallback) {
        getGigTicket(id: "4", callback: callback)
    }
    
    func getDoneTicket(callback: BasicTicketCallback) {
        getGigTicket(id: "5", callback: callback)
    }
    
    func getWaitConfirmBookTicket(callback: BasicConfirmTicketCallback) {
        getGigBookTicket(id: "13", callback: callback)
    }
    
    func getWaitConfirmPickTicket(callback: BasicConfirmTicketCallback) {
        getGigBookTicket(id: "14", callback: callback)
    }
    
    func getProcessingGigTicket(callback: BasicTicketCallback) {
        getGigTicket(id: "3", callback: callback)
    }
    
    func getGigTicket(id: String, callback: BasicTicketCallback) {
        var params : [Param] = []
        params.append(Param(key: "_status", value: id))
        params.append(Param(key: "_request", value: "get_gig_ticket"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            do {
                var returnData : [ResponseGigTicket] = []
                let dataEncode = data.data(using: String.Encoding.utf8)
                let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : AnyObject]
                
                let dataValue = dataJson["data"] as! [AnyObject]
                for d in dataValue {
                    let da = d as! [String : String]
                    var dd = ResponseGigTicket()
                    dd.id = da["id"]
                    dd.name = da["name"]
                    dd.size = da["size"]
                    dd.from = da["address"]
                    dd.to = da["address2"]
                    dd.note = da["note"]
                    dd.rate = da["rate"]
                    
                    returnData.append(dd)
                }
                callback.onSuccess(data: returnData)
            }
            catch {
                callback.onFail()
            }
        }
        
        mNetwork?.onError = {
            error in
            callback.onFail()
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func getGigBookTicket(id: String, callback: BasicConfirmTicketCallback) {
        var params : [Param] = []
        params.append(Param(key: "_status", value: id))
        params.append(Param(key: "_request", value: "get_gig_confirm_ticket"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            do {
                var returnData : [ResponseGigConfirmTicket] = []
                let dataEncode = data.data(using: String.Encoding.utf8)
                let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : AnyObject]
                
                let dataValue = dataJson["data"] as! [AnyObject]
                for d in dataValue {
                    let da = d as! [String : String]
                    var dd = ResponseGigConfirmTicket()
                    dd.id = da["id"]
                    dd.name = da["name"]
                    dd.size = da["size"]
                    dd.from = da["address"]
                    dd.to = da["address2"]
                    dd.size = da["shipper"]
                    
                    returnData.append(dd)
                }
                callback.onSuccess(data: returnData)
            }
            catch {
                callback.onGetConfirmFail()
            }
        }
        
        mNetwork?.onError = {
            error in
            callback.onGetConfirmFail()
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func getGigTicketDetail(id: String, callback: GetGigDetailCallback) {
        var params : [Param] = []
        params.append(Param(key: "_gig_id", value: id))
        params.append(Param(key: "_request", value: "get_gig_ticket_detail"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            do {
                let dataEncode = data.data(using: String.Encoding.utf8)
                let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : String]
                
                var gigDetail = ResponseGigTicketDetail()
                gigDetail.id = dataJson["id"]
                gigDetail.receiverName = dataJson["receiver_name"]
                gigDetail.receiverPhone = dataJson["phone"]
                gigDetail.receiverAddress = dataJson["address2"]! + ", "
                gigDetail.receiverAddress = gigDetail.receiverAddress! + dataJson["ward"]! + ", "
                gigDetail.receiverAddress = gigDetail.receiverAddress! + dataJson["district"]! + ", "
                gigDetail.receiverAddress = gigDetail.receiverAddress! + dataJson["city"]!
                
                gigDetail.receiverJustAddress = dataJson["address2"]!
                gigDetail.receiverProvince = dataJson["city"]!
                gigDetail.receiverDistrict = dataJson["district"]!
                gigDetail.receiverWard = dataJson["ward"]!
                
                gigDetail.ticketName = dataJson["name"]
                gigDetail.ticketWeight = dataJson["weight"]
                gigDetail.ticketSize = dataJson["size"]
                gigDetail.ticketPaymentMethod = dataJson["payment_method"]
                gigDetail.ticketPickDate = dataJson["date"]
                gigDetail.ticketCOD = dataJson["cod"]
                gigDetail.ticketNote = dataJson["note"]
                gigDetail.senderName = self.mCurrentUser?.name
                gigDetail.senderPhone = self.mCurrentUser?.phone
                gigDetail.senderAdress = dataJson["address"]
                gigDetail.senderBankName = dataJson["bank_name"]
                gigDetail.senderBankNumber = dataJson["bank_number"]
                gigDetail.senderBankBranch = dataJson["bank_bank"]
                gigDetail.shipperID = dataJson["shipper_id"]
                
                callback.onSuccess(data: gigDetail)
            }
            catch {
                callback.onError()
            }
        }
        
        mNetwork?.onError = {
            error in
            callback.onError()
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func deleteTicket(id: String, callback: BasicTrueFailCallback) {
        var params : [Param] = []
        params.append(Param(key: "_gig_id", value: id))
        params.append(Param(key: "_request", value: "delete_gig_ticket"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            callback.onSuccess(type: GigDetailViewController.CALLBACK_TYPE.delete)
        }
        
        mNetwork?.onError = {
            error in
            callback.onFail()
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func cancelTicket(id: String, callback: BasicTrueFailCallback) {
        var params : [Param] = []
        params.append(Param(key: "_gig_id", value: id))
        params.append(Param(key: "_request", value: "cancel_gig_ticket"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            callback.onSuccess(type: GigDetailViewController.CALLBACK_TYPE.cancel)
        }
        
        mNetwork?.onError = {
            error in
            callback.onFail()
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func rating(id: String, point: String, callback: BasicTrueFailCallback) {
        var params : [Param] = []
        params.append(Param(key: "_gigid", value: id))
        params.append(Param(key: "_rate", value: point))
        params.append(Param(key: "_request", value: "rating"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            callback.onSuccess(type: GigDetailViewController.CALLBACK_TYPE.cancel)
        }
        
        mNetwork?.onError = {
            error in
            callback.onFail()
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func getShipperInfo(id: String, callback: GetShipperCallback) {
        var params : [Param] = []
        params.append(Param(key: "_shipperid", value: id))
        params.append(Param(key: "_request", value: "get_shipper_info"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            do {
                let dataEncode = data.data(using: String.Encoding.utf8)
                let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : AnyObject]
                
                var shipper = Shipper()
                shipper.id = dataJson["id"] as? String
                shipper.account = dataJson["account"] as? String
                shipper.email = dataJson["email"] as? String
                shipper.name = dataJson["name"] as? String
                shipper.phone = dataJson["phone"] as? String
                shipper.rate = String(dataJson["current_rating"] as! Int)
                shipper.avatar = dataJson["avatar"] as? String
                shipper.totalRating = String(dataJson["total_rating"] as! Int)
                
                callback.onSuccess(data: shipper)
            }
            catch {
                callback.onError()
            }
        }
        
        mNetwork?.onError = {
            error in
            callback.onError()
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
    
    func getTicketPosition(id: String, callback: TrackingPositionCallback) {
        var params : [Param] = []
        params.append(Param(key: "_gig_id", value: id))
        params.append(Param(key: "_request", value: "get_gig_position"))
        params.append(Param(key: "_id", value: (mCurrentUser?.accountID)!))
        params.append(Param(key: "_token", value: (mCurrentUser?.token)!))
        
        mNetwork?.onComplete = {
            data in
            do {
                let dataEncode = data.data(using: String.Encoding.utf8)
                let dataJson = try JSONSerialization.jsonObject(with: dataEncode!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String : AnyObject]
                
                var position = ResponsePosition()
                position.lat = dataJson["latitude"] as? Double
                position.lon = dataJson["longitude"] as? Double
                
                callback.onSuccess(data: position)
            }
            catch {
                callback.onError()
            }
        }
        
        mNetwork?.onError = {
            error in
            callback.onError()
        }
        
        mNetwork?.execute(link: Constants.HOST_API, params: params)
    }
}

extension FSService : LoginCallback {
    func onSuccess(user: User) {
        self.setCurrentUser(user: user)
    }
    
    func onError(error: String) {
        LoginManager().logOut()
        self.checkLoginUser()
    }
}

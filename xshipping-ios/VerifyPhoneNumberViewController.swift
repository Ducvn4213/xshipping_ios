import UIKit
import AccountKit

protocol VerifyPhoneNumberCallback {
    func onSucess()
    func onError()
}

class VerifyPhoneNumberViewController : UIViewController, AKFViewControllerDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var accountKit: AKFAccountKit!
    @IBOutlet weak var mConfirmButton: UIButton!
    @IBOutlet weak var mSkipButton: UIButton!
    var hostNavController : UINavigationController!
    var mService : FSService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hostNavController = appDelegate.getHostNavController()
        mService = appDelegate.getFSService()
        
        if accountKit == nil {
            // may also specify AKFResponseTypeAccessToken
            self.accountKit = AKFAccountKit(responseType: AKFResponseType.accessToken)
        }
        
        configUI()
    }
    
    func configUI() {
        mConfirmButton.layer.cornerRadius = 4
        mSkipButton.layer.cornerRadius = 4
        
        mConfirmButton.layer.masksToBounds = true
        mConfirmButton.layer.masksToBounds = true
    }
    
    @IBAction func onSkip(_ sender: Any) {
        //TODO: move to send gig
    }
    
    func viewController(_ viewController: UIViewController!, didCompleteLoginWith accessToken: AKFAccessToken!, state: String!) {
        accountKit.requestAccount{
            (account, error) -> Void in
            
            if account?.phoneNumber?.phoneNumber != nil {
                let phone = account!.phoneNumber?.stringRepresentation()
                self.mService?.verify(phone: phone!, callback: self)
            }
        }
    }
    func viewController(_ viewController: UIViewController!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
        print("Login succcess with AuthorizationCode")
    }
    private func viewController(_ viewController: UIViewController!, didFailWithError error: NSError!) {
        let user = mService?.getCurrentUser()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "sendgig")
        self.hostNavController.viewControllers = [someVC]
        
        Utils.showDialog(host: self.hostNavController, title: "Tìm Shipper", message: "Hi " + (user?.name!)! + ", chào mừng bạn đên với chúng tôi. Tài khoản của bạn chưa được xác thực số điện thoại, hãy xác thực trong mục 'Profile' để sử dụng tất cả các tính năng của chúng tôi")
    }
    func viewControllerDidCancel(_ viewController: UIViewController!) {
        let user = mService?.getCurrentUser()

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "sendgig")
        self.hostNavController.viewControllers = [someVC]
        
        Utils.showDialog(host: self.hostNavController, title: "Tìm Shipper", message: "Hi " + (user?.name!)! + ", chào mừng bạn đên với chúng tôi. Tài khoản của bạn chưa được xác thực số điện thoại, hãy xác thực trong mục 'Profile' để sử dụng tất cả các tính năng của chúng tôi")
    }
    
    func prepareLoginViewController(_ verifyPhoneNumberViewController: AKFViewController) {
        verifyPhoneNumberViewController.delegate = self
        verifyPhoneNumberViewController.setAdvancedUIManager(nil)
        verifyPhoneNumberViewController.setTheme(nil)
    }
    
    @IBAction func onConfirm(_ sender: Any) {
        let inputState: String = UUID().uuidString
        let prePhoneNumber = AKFPhoneNumber(countryCode: "+84", phoneNumber: "")
        let viewController:AKFViewController = accountKit.viewControllerForPhoneLogin(with: prePhoneNumber, state: inputState)  as! AKFViewController
        viewController.enableSendToFacebook = true
        self.prepareLoginViewController(viewController)
        self.present(viewController as! UIViewController, animated: true, completion: nil)
    }
}

extension VerifyPhoneNumberViewController : VerifyPhoneNumberCallback {
    func onSucess() {
        mService?.verifySuccessAccount()
        let user = mService?.getCurrentUser()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "sendgig")
        self.hostNavController.viewControllers = [someVC]
        
        Utils.showDialog(host: self.hostNavController, title: "Tìm Shipper", message: "Hi " + (user?.name!)! + ", chào mừng bạn đã đến với chúng tôi")
    }
    
    func onError() {
        let user = mService?.getCurrentUser()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "sendgig")
        self.hostNavController.viewControllers = [someVC]
        
        Utils.showDialog(host: self.hostNavController, title: "Tìm Shipper", message: "Hi " + (user?.name!)! + ", chào mừng bạn đên với chúng tôi. Tài khoản của bạn chưa được xác thực số điện thoại, hãy xác thực trong mục 'Profile' để sử dụng tất cả các tính năng của chúng tôi")
    }
}

import UIKit
import FacebookLogin
import FacebookCore

class MenuTableViewController : UITableViewController {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var hostNavController : UINavigationController!
    var mService : FSService?
    
    @IBOutlet weak var mLoginButton: UIButton!
    @IBOutlet weak var mLoginImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hostNavController = appDelegate.getHostNavController()
        self.mService = appDelegate.getFSService()
        
        updateUI()
    }
    
    func updateUI() {
        if mService?.getCurrentUser() != nil {
            mLoginButton.setTitle("Đăng xuất", for: .normal)
            mLoginImage.image = UIImage(named: "logout_bg")
        }
        else {
            mLoginButton.setTitle("Đăng nhập", for: .normal)
            mLoginImage.image = UIImage(named: "login_bg")
        }
    }
    @IBAction func onWaitBook(_ sender: Any) {
        appDelegate.curretnPresentView = .wait_book
        goto(indentifier: "waitbook")
    }
    
    @IBAction func onWaitPick(_ sender: Any) {
        appDelegate.curretnPresentView = .wait_pick
        goto(indentifier: "waitpick")
    }
    
    @IBAction func onProcessing(_ sender: Any) {
        appDelegate.curretnPresentView = .processing
        goto(indentifier: "processing")
    }
    
    @IBAction func onDone(_ sender: Any) {
        appDelegate.curretnPresentView = .done
        goto(indentifier: "done")
    }
    
    @IBAction func onLogin(_ sender: Any) {
        if mService?.getCurrentUser() != nil {
            let alert = UIAlertController(title: "Thông Báo", message: "Bạn có chắc chắn muốn đăng xuất không?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Có", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
                LoginManager().logOut()
                self.mService?.setCurrentUser(user: nil)
                self.mService?.saveString(string: "", key: Constants.SHARE_PREFERENCE_USER_KEY)
                if (self.appDelegate.curretnPresentView == .profile) {
                    self.appDelegate.curretnPresentView = .send
                    self.goto(indentifier: "sendgig")
                }
                self.dismiss(animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Không", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        appDelegate.curretnPresentView = .login
        goto(indentifier: "login")
    }
    @IBAction func onSendGig(_ sender: Any) {
        appDelegate.curretnPresentView = .send
        mService?.setEditingTicketObject(data: nil)
        goto(indentifier: "sendgig")
    }
    @IBAction func onNews(_ sender: Any) {
        appDelegate.curretnPresentView = .news
        Utils.showDialog(host: self, title: "Thông báo", message: "Tính năng này đang được phát triển")
    }
    @IBAction func onFeedback(_ sender: Any) {
        appDelegate.curretnPresentView = .feedback
        Utils.showDialog(host: self, title: "Thông báo", message: "Tính năng này đang được phát triển")
    }
    @IBAction func onStatistic(_ sender: Any) {
        appDelegate.curretnPresentView = .statistic
        Utils.showDialog(host: self, title: "Thông báo", message: "Tính năng này đang được phát triển")
    }
    @IBAction func onProfile(_ sender: Any) {
        if (self.mService?.getCurrentUser() == nil) {
            appDelegate.curretnPresentView = .login
            goto(indentifier: "login")
            return
        }
        
        appDelegate.curretnPresentView = .profile
        goto(indentifier: "profile")
    }
    
    func goto(indentifier : String) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: indentifier)
        self.hostNavController.viewControllers = [someVC]
        self.dismiss(animated: true, completion: nil)
    }
}

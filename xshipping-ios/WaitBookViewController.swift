import UIKit

protocol BasicTicketCallback {
    func onSuccess(data: [ResponseGigTicket])
    func onFail()
}

protocol BasicConfirmTicketCallback {
    func onSuccess(data: [ResponseGigConfirmTicket])
    func onGetConfirmFail()
}

class WaitBookViewController : UIViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    
    
    @IBOutlet weak var mSegment: UISegmentedControl!
    @IBOutlet weak var mTableView: UITableView!
    var mData : [ResponseGigTicket] = []
    var mData_2 : [ResponseGigConfirmTicket] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mService = appDelegate.getFSService()
            }
    
    @IBAction func segmentChanged(_ sender: Any) {
        let host = sender as! UISegmentedControl
        if (host.selectedSegmentIndex == 0) {
            mService?.getWaitBookTicket(callback: self)
        }
        else {
            mService?.getWaitConfirmBookTicket(callback: self)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (mSegment.selectedSegmentIndex == 0) {
            mService?.getWaitBookTicket(callback: self)
        }
        else {
            mService?.getWaitConfirmBookTicket(callback: self)
        }    }
}

extension WaitBookViewController : BasicTicketCallback {
    func onSuccess(data: [ResponseGigTicket]) {
        mData = data
        mTableView.reloadData()
    }
    
    func onFail() {
        Utils.showDialog(host: self, title: "Tìm Shipper", message: "Hiện tại không có đơn hàng chờ đặt nào")
    }
}

extension WaitBookViewController : BasicConfirmTicketCallback {
    func onSuccess(data: [ResponseGigConfirmTicket]) {
        mData_2 = data
        mTableView.reloadData()
    }
    
    func onGetConfirmFail() {
        Utils.showDialog(host: self, title: "Tìm Shipper", message: "Hiện tại không có đơn hàng chờ xác nhận nào")
    }
}

extension WaitBookViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (mSegment.selectedSegmentIndex == 0) {
            return mData.count
        }
        else {
            return mData_2.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (mSegment.selectedSegmentIndex == 0) {
            let cellIdentifier = "Cell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BasicTicketCell
            
            let data = mData[indexPath.row]
            
            cell.mName.text = data.name
            cell.mSize.text = data.size
            cell.mFrom.text = data.from
            cell.mTo.text = data.to
            
            cell.selectionStyle = .none
            return cell
        }
        else {
            let cellIdentifier = "Cell_2"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BasicTicketWithRedLabelCell
            
            let data = mData_2[indexPath.row]
            
            cell.mName.text = data.name
            cell.mSize.text = data.size
            cell.mFrom.text = data.from
            cell.mTo.text = data.to
            if (data.shipper != nil) {
                let shipperlabel : String = data.shipper!
                cell.mRedLabel.text = shipperlabel + " muốn giao đơn hàng này"
            }
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (mSegment.selectedSegmentIndex == 0) {
            return 100
        }
        else {
            return 120
        }
    }
}

extension WaitBookViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "gigdetail") as! GigDetailViewController
        
        if (mSegment.selectedSegmentIndex == 0) {
            someVC.gigID = mData[indexPath.row].id
            someVC.ticketType = .waitBook
        }
        else {
            someVC.gigID = mData_2[indexPath.row].id
            someVC.ticketType = .waitBookConfirm
        }
        
        self.navigationController?.pushViewController(someVC, animated: true)
    }
}

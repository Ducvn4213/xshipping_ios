import UIKit
import Cosmos

class RatingViewController : UIViewController {
    @IBOutlet weak var mRatingBar: CosmosView!
    @IBOutlet weak var mSubmit: UIButton!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    
    var ticketID : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mService = appDelegate.getFSService()
        
        correctUI()
    }
    
    func correctUI() {
        self.mSubmit.layer.cornerRadius = 3
        self.mSubmit.layer.masksToBounds = true
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        let point = self.mRatingBar.rating
        
        mService?.rating(id: ticketID, point: String(Int(point)), callback: self)
    }
}

extension RatingViewController : BasicTrueFailCallback {
    func onSuccess(type: GigDetailViewController.CALLBACK_TYPE) {
        let alert = UIAlertController(title: "Tìm Shipper", message: "Cảm ơn bạn đã đưa ra đánh giá", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func onFail() {
        Utils.showDialog(host: self, title: "Tìm Shipper", message: "Có lỗi, vui lòng thử lại")
    }
}

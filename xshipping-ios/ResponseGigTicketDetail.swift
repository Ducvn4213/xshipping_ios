struct ResponseGigTicketDetail {
    var id : String?
    
    var receiverName : String?
    var receiverPhone : String?
    var receiverAddress : String?
    var receiverJustAddress : String?
    var receiverDistrict : String?
    var receiverWard : String?
    var receiverProvince : String?
    
    
    var senderName : String?
    var senderPhone : String?
    var senderAdress : String?
    var senderBankName : String?
    var senderBankNumber : String?
    var senderBankBranch : String?
    
    var ticketName : String?
    var ticketWeight : String?
    var ticketSize : String?
    var ticketPaymentMethod : String?
    var ticketPickDate : String?
    var ticketCOD : String?
    var ticketNote : String?
    
    var shipperID : String?
    
    init() {}
}

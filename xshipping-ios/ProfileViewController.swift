import UIKit

protocol UpdateProfileCallback {
    func onSuccess(data: User)
    func onError()
}

protocol UpdateAvatarCallback {
    func onSuccess()
    func onAvatarError()
}

class ProfileViewController : UITableViewController {
    
    var picker : UIImagePickerController? = UIImagePickerController()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    var mImageString : String = ""
    
    @IBOutlet weak var mAvatar: UIImageView!
    @IBOutlet weak var mBigName: UILabel!
    @IBOutlet weak var mBigEmail: UILabel!
    @IBOutlet weak var nName: UITextField!
    @IBOutlet weak var mEmail: UITextField!
    @IBOutlet weak var mPhone: UITextField!
    @IBOutlet weak var mAddress: UITextField!
    @IBOutlet weak var mBankName: UITextField!
    @IBOutlet weak var mBankNumber: UITextField!
    @IBOutlet weak var mBankBranch: UITextField!
    @IBOutlet weak var mVerifyPhoneNumberButton: UIButton!
    @IBOutlet weak var mChangePasswordButton: UIButton!
    @IBOutlet weak var menuAction: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mService = appDelegate.getFSService()
        picker?.delegate = self
        
        configUI()
        loadData()
    }
    
    func configUI() {
        mVerifyPhoneNumberButton.layer.cornerRadius = 4
        mVerifyPhoneNumberButton.layer.masksToBounds = true
        
        mChangePasswordButton.layer.cornerRadius = 4
        mChangePasswordButton.layer.masksToBounds = true
    }
    
    func loadData() {
        let user = mService?.getCurrentUser()
        
        mBigName.text = user?.name
        mBigEmail.text = user?.email
        nName.text = user?.name
        mEmail.text = user?.email
        mPhone.text = user?.phone
        mAddress.text = user?.address
        mBankName.text = user?.bankName
        mBankNumber.text = user?.bankNumber
        mBankBranch.text = user?.bankBranch
        if (user?.avatar != nil) {
            Utils.loadImageFor(container: mAvatar, link: (user?.avatar)!)
        }
        
        if (user?.isActive == "1") {
            mVerifyPhoneNumberButton.isHidden = true
        }
        else {
            mVerifyPhoneNumberButton.isHidden = false
        }
    }
    
    @IBAction func requestEdit(_ sender: Any) {
        if (menuAction.title == "Cập nhật") {
            menuAction.title = "Chỉnh sửa"
            updateData()
        }
        else {
            menuAction.title = "Cập nhật"
            changeToEditMode()
        }
    }
    
    func updateData() {
        nName.isEnabled = false
        mPhone.isEnabled = false
        mAddress.isEnabled = false
        mBankName.isEnabled = false
        mBankNumber.isEnabled = false
        mBankName.isEnabled = false
        
        let newName = nName.text
        let newPhone = mPhone.text
        let newAddress = mAddress.text
        let newBankName = mBankName.text
        let newBankNumber = mBankNumber.text
        let newBankBranch = mBankBranch.text
        
        if (newName == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Họ và tên không thể trống")
            return
        }
        if (newPhone == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Số điện thoại không thể trống")
            return
        }
        if (newAddress == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Địa chỉ không thể trống")
            return
        }
        
        mService?.updateProfile(name: newName!, phone: newPhone!, address: newAddress!, bname: newBankName!, bnumber: newBankNumber!, bbranch: newBankBranch!, callback: self)
    }
    
    func changeToEditMode() {
        nName.isEnabled = true
        mPhone.isEnabled = true
        mAddress.isEnabled = true
        mBankName.isEnabled = true
        mBankNumber.isEnabled = true
        mBankName.isEnabled = true
        
        nName.becomeFirstResponder()
    }
    
    @IBAction func onChangeImage(_ sender: Any) {
        if (mAddress.isEnabled == false) {
            Utils.showDialog(host: self, title: "Tìm Shipper", message: "Chọn chỉnh sửa trên thanh công cụ để kích hoạt thay đổi hình đại diện của bạn")
        }
        else {
            openGallary()
        }
    }
    
    func openGallary() {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(picker!, animated: true, completion: nil)
    }
}

extension ProfileViewController : UpdateProfileCallback {
    func onSuccess(data: User) {
        if (self.mImageString != "") {
            mService?.updateAvatar(stringEncode: self.mImageString, callback: self)
        }
        Utils.showDialog(host: self, title: "Tìm Shipper", message: "Cập nhật thông tin thành công")
    }
    
    func onError() {
        Utils.showDialog(host: self, title: "Có Lỗi", message: "Có lỗi khi cập nhật thông tin. Vui lòng thử lại")
        loadData()
    }
}

extension ProfileViewController : UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let imageData = UIImagePNGRepresentation(chosenImage)
        self.mImageString = (imageData?.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters))!
        
        self.mAvatar.image = chosenImage
        dismiss(animated: true, completion: nil)
    }
}

extension ProfileViewController : UpdateAvatarCallback {
    func onSuccess() {
        //TODO
    }
    
    func onAvatarError() {
        //TODO
    }
}

extension ProfileViewController : UINavigationControllerDelegate {
    
}

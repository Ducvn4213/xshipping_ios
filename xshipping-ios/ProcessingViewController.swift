import UIKit

class ProcessingViewController : UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    
    @IBOutlet weak var mTableView: UITableView!
    var mData : [ResponseGigTicket] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mService = appDelegate.getFSService()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        mService?.getProcessingGigTicket(callback: self)
    }
}

extension ProcessingViewController : BasicTicketCallback {
    func onSuccess(data: [ResponseGigTicket]) {
        mData = data
        mTableView.reloadData()
    }
    
    func onFail() {
        Utils.showDialog(host: self, title: "Tìm Shipper", message: "Hiện tại không có đơn hàng chờ lấy nào")
    }
}

extension ProcessingViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BasicTicketCell
        
        let data = mData[indexPath.row]
        
        cell.mName.text = data.name
        cell.mSize.text = data.size
        cell.mFrom.text = data.from
        cell.mTo.text = data.to
        
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

extension ProcessingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "gigdetail") as! GigDetailViewController
        
        someVC.gigID = mData[indexPath.row].id
        someVC.ticketType = .processing
        self.navigationController?.pushViewController(someVC, animated: true)
    }
}

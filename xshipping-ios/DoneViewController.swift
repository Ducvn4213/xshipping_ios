import UIKit

class DoneViewController : UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    
    
    @IBOutlet weak var mSegment: UISegmentedControl!
    @IBOutlet weak var mTableView: UITableView!
    var mData : [ResponseGigTicket] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mService = appDelegate.getFSService()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if (mSegment.selectedSegmentIndex == 0) {
            mService?.getErrorTicket(callback: self)
        }
        else if (mSegment.selectedSegmentIndex == 1) {
            mService?.getWaitCODTicket(callback: self)
        }
        else {
            mService?.getDoneTicket(callback: self)
        }
    }
    
    @IBAction func segmentChanged(_ sender: Any) {
        let host = sender as! UISegmentedControl
        if (host.selectedSegmentIndex == 0) {
            mService?.getErrorTicket(callback: self)
        }
        else if (host.selectedSegmentIndex == 1) {
            mService?.getWaitCODTicket(callback: self)
        }
        else {
            mService?.getDoneTicket(callback: self)
        }
    }
}

extension DoneViewController : BasicTicketCallback {
    func onSuccess(data: [ResponseGigTicket]) {
        mData = data
        mTableView.reloadData()
    }
    
    func onFail() {
        Utils.showDialog(host: self, title: "Có Lỗi", message: "Không thể lấy dữ liệu")
    }
}

extension DoneViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell_2"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BasicTicketWithRedLabelAndStartCell
        
        let data = mData[indexPath.row]
        
        cell.mName.text = data.name
        cell.mSize.text = data.size
        cell.mFrom.text = data.from
        cell.mTo.text = data.to
        if (data.note == "") {
            cell.mRedLabel.isHidden = true
        }
        else {
            cell.mRedLabel.isHidden = false
            if (mSegment.selectedSegmentIndex == 0) {
                cell.mRedLabel.text = "Nguyên nhân lỗi: " + data.note!
            }
            else {
                cell.mRedLabel.text = "Note từ người giao hàng: " + data.note!
            }
        }
        
        let rate = Int(data.rate!)
        if (rate == 0) {
            cell.mStar.isHidden = true
        }
        else {
            cell.mStar.isHidden = false
            cell.mStar.rating = Double(rate!)
        }
        
        cell.mStar.settings.updateOnTouch = false
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
}

extension DoneViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "gigdetail") as! GigDetailViewController
        
        someVC.gigID = mData[indexPath.row].id
        if (Int(mData[indexPath.row].rate!)! == 0) {
            someVC.isRated = false
        }
        else {
            someVC.isRated = true
        }
        
        if (mSegment.selectedSegmentIndex == 0) {
            someVC.ticketType = .error
        }
        else if (mSegment.selectedSegmentIndex == 1) {
            someVC.ticketType = .error
        }
        else {
            someVC.ticketType = .error
        }
        self.navigationController?.pushViewController(someVC, animated: true)
    }
}

struct User {
    var id : String?
    var accountID : String?
    var address : String?
    var email: String?
    var name : String?
    var phone : String?
    var isActive : String?
    var avatar : String?
    var token : String?
    var bankName : String?
    var bankNumber : String?
    var bankBranch : String?
    
    init() {}
}

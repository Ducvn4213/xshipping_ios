import UIKit

class EditGigViewController : UITableViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    
    @IBOutlet weak var mReceiverName: UITextField!
    @IBOutlet weak var mReceiverPhone: UITextField!
    @IBOutlet weak var mReceiverAddress: UITextField!
    @IBOutlet weak var mReceiveProvinceButton: UIButton!
    @IBOutlet weak var mReceiverDistrictButton: UIButton!
    @IBOutlet weak var mReceiverWardButton: UIButton!
    
    @IBOutlet weak var mSenderName: UITextField!
    @IBOutlet weak var mSenderPhone: UITextField!
    @IBOutlet weak var mSenderAddress: UITextField!
    @IBOutlet weak var mSenderProvinceButton: UIButton!
    @IBOutlet weak var mSenderDistrictButton: UIButton!
    @IBOutlet weak var mSenderWardButton: UIButton!
    
    @IBOutlet weak var mGigName: UITextField!
    @IBOutlet weak var mGigWeight: UITextField!
    @IBOutlet weak var mGigSizeButton: UIButton!
    @IBOutlet weak var mGigPaymentMethodButton: UIButton!
    @IBOutlet weak var mGigPickDateButton: UIButton!
    @IBOutlet weak var mGigCOD: UITextField!
    @IBOutlet weak var mGigNote: UITextView!
    
    var mResponseGigDetail : ResponseGigTicketDetail?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        configEvent()
    }
    
    func configEvent() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func loadData() {
        if (self.mResponseGigDetail != nil) {
            mReceiverName.text = (self.mResponseGigDetail?.receiverName)!
            mReceiverPhone.text = (self.mResponseGigDetail?.receiverPhone)!
            mReceiverAddress.text = "Địa chỉ: " + (self.mResponseGigDetail?.receiverAddress)!
            mSenderName.text = (self.mResponseGigDetail?.senderName)!
            mSenderPhone.text = (self.mResponseGigDetail?.senderPhone!)!
            mSenderAddress.text = "Địa chỉ: " + (self.mResponseGigDetail?.senderAdress!)!
            mGigName.text = (self.mResponseGigDetail?.ticketName!)!
            mGigWeight.text = (self.mResponseGigDetail?.ticketWeight!)!
            mGigSizeButton.setTitle((self.mResponseGigDetail?.ticketSize!)!, for: .normal)
            mGigPaymentMethodButton.setTitle((self.mResponseGigDetail?.ticketPaymentMethod!)!, for: .normal)
            mGigPickDateButton.setTitle("Ngày lấy hàng: " + (self.mResponseGigDetail?.ticketPickDate!)!, for: .normal)
            mGigCOD.text = ((self.mResponseGigDetail?.ticketCOD!)! == "" ? "không thu hộ" : (self.mResponseGigDetail?.ticketCOD!)!)
            mGigNote.text = ((self.mResponseGigDetail?.ticketNote!)! == "" ? "không có ghi chú" : (self.mResponseGigDetail?.ticketNote!)!)

        }
    }
    
    @IBAction func onRequestChangeReceiverProvince(_ sender: Any) {
    }
    @IBAction func onRequestChangeReceiverDistrict(_ sender: Any) {
    }
    @IBAction func onRequestChangeReceiverWard(_ sender: Any) {
    }
    @IBAction func onRequestChangeSenderProvince(_ sender: Any) {
    }
    @IBAction func onRequestChangeSenderDistrict(_ sender: Any) {
    }
    @IBAction func onRequestChangeSenderWard(_ sender: Any) {
    }
}

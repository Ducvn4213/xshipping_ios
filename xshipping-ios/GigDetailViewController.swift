import UIKit

protocol GetGigDetailCallback {
    func onSuccess(data: ResponseGigTicketDetail)
    func onError()
}

protocol BasicTrueFailCallback {
    func onSuccess(type: GigDetailViewController.CALLBACK_TYPE)
    func onFail()
}

class GigDetailViewController : UITableViewController {
    
    enum TICKET_TYPE {
        case waitBook
        case waitBookConfirm
        case waitPick
        case waitPickConfirm
        case processing
        case error
        case doneWaitCOD
        case done
        case none
    }
    
    enum CALLBACK_TYPE {
        case delete
        case update
        case cancel
    }
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    var responseGigDetail : ResponseGigTicketDetail?
    
    @IBOutlet weak var mReceiverName: UILabel!
    @IBOutlet weak var mReceiverPhone: UILabel!
    @IBOutlet weak var mReceiverAddress: UILabel!
    @IBOutlet weak var mSenderName: UILabel!
    @IBOutlet weak var mSenderPhone: UILabel!
    @IBOutlet weak var mSenderAddress: UILabel!
    @IBOutlet weak var mTicketName: UILabel!
    @IBOutlet weak var mTicketWeight: UILabel!
    @IBOutlet weak var mTicketSize: UILabel!
    @IBOutlet weak var mTicketPaymentMethod: UILabel!
    @IBOutlet weak var mTicketPickDate: UILabel!
    @IBOutlet weak var mTicketCOD: UILabel!
    @IBOutlet weak var mTicketNote: UILabel!
    @IBOutlet weak var mActionControl: UIBarButtonItem!
    
    var gigID : String?
    var ticketType : TICKET_TYPE = .none
    var isRated : Bool = false
    var shipperID : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mService = appDelegate.getFSService()
        
        mActionControl.isEnabled = false
        mActionControl.tintColor = UIColor.clear
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        mService?.getGigTicketDetail(id: gigID!, callback: self)
        
        switch ticketType {
        case .waitBookConfirm:
            mActionControl.isEnabled = false
            mActionControl.tintColor = UIColor.clear
            break
        case .waitPickConfirm:
            mActionControl.isEnabled = false
            mActionControl.tintColor = UIColor.clear
            break
        default:
            mActionControl.isEnabled = true
            mActionControl.tintColor = UIColor.white
            break
        }
    }
    
    @IBAction func onOptions(_ sender: Any) {
        
        let actionUpdate = UIAlertAction(title: "Chỉnh sửa", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.goToEdit()
        })
        let actionDelete = UIAlertAction(title: "Xoá vận đơn", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.performDeleteDialog()
        })
        let actionViewShipper = UIAlertAction(title: "Xem thông tin Shipper", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.viewShipper()
        })
        let actionTracking = UIAlertAction(title: "Theo dõi đơn hàng", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.goToTracking()
        })
        let actionCancel = UIAlertAction(title: "Huỷ vận đơn", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.performCancelDialog()
        })
        let actionRate = UIAlertAction(title: "Đánh giá Shipper", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.goToRate()
        })
        
        let alert = UIAlertController(title: "Chọn hành động", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        switch ticketType {
        case .waitBook:
            alert.addAction(actionUpdate)
            alert.addAction(actionDelete)
            break
        case .waitBookConfirm: break
            //hide
        case .waitPick:
            alert.addAction(actionViewShipper)
            alert.addAction(actionDelete)
            break
        case .waitPickConfirm: break
            //hide
        case .processing:
            alert.addAction(actionTracking)
            alert.addAction(actionCancel)
            alert.addAction(actionViewShipper)
            break
        case .error:
            alert.addAction(actionDelete)
            alert.addAction(actionViewShipper)
            if (!isRated) {
                alert.addAction(actionRate)
            }
            break
        case .doneWaitCOD:
            alert.addAction(actionDelete)
            alert.addAction(actionViewShipper)
            if (!isRated) {
                alert.addAction(actionRate)
            }
            break
        case .done:
            alert.addAction(actionDelete)
            alert.addAction(actionViewShipper)
            if (!isRated) {
                alert.addAction(actionRate)
            }
            break
        default:
            break
        }
        
        alert.addAction(UIAlertAction(title: "Huỷ", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func performDeleteDialog() {
        let alert = UIAlertController(title: "Tìm Shipper", message: "Bạn có muốn xoá vận đơn này?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Có", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.mService?.deleteTicket(id: self.gigID!, callback: self)
        }))
        alert.addAction(UIAlertAction(title: "Huỷ", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func performCancelDialog() {
        let alert = UIAlertController(title: "Tìm Shipper", message: "Bạn có muốn huỷ vận đơn này?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Có", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.mService?.cancelTicket(id: self.gigID!, callback: self)
        }))
        alert.addAction(UIAlertAction(title: "Bỏ qua", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func goToEdit() {
        mService?.setEditingTicketObject(data: self.responseGigDetail!)
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "editsendgig")
        self.navigationController?.pushViewController(someVC, animated: true)
    }
    
    func goToTracking() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "tracking") as! TrackingViewController
        someVC.gigID = gigID!
        self.navigationController?.pushViewController(someVC, animated: true)
    }
    
    func viewShipper() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "viewshipper") as! ViewShipperViewController
        someVC.shipperID = self.shipperID
        self.navigationController?.pushViewController(someVC, animated: true)
        
    }
    
    func goToRate() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "rating") as! RatingViewController
        someVC.ticketID = gigID!
        self.navigationController?.pushViewController(someVC, animated: true)
    }
}

extension GigDetailViewController : BasicTrueFailCallback {
    func onSuccess(type: CALLBACK_TYPE) {
        switch type {
        case .delete:
            let alert = UIAlertController(title: "Tìm Shipper", message: "Bạn đã xoá thành công vận đơn", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
            break
        case .cancel:
            let alert = UIAlertController(title: "Tìm Shipper", message: "Bạn đã huỷ thành công vận đơn", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
            break
        case .update:
            break
        }
    }
    
    func onFail() {
        Utils.showDialog(host: self, title: "Tìm Shipper", message: "Có lỗi, vui lòng thử lại")
    }
}

extension GigDetailViewController : GetGigDetailCallback {
    func onSuccess(data: ResponseGigTicketDetail) {
        self.responseGigDetail = data
        mReceiverName.text = "Tên: " + data.receiverName!
        mReceiverPhone.text = "Số điện thoại: " + data.receiverPhone!
        mReceiverAddress.text = "Địa chỉ: " + data.receiverAddress!
        mSenderName.text = "Tên: " + data.senderName!
        mSenderPhone.text = "Số điện thoại: " + data.senderPhone!
        mSenderAddress.text = "Địa chỉ: " + data.senderAdress!
        mTicketName.text = "Tên: " + data.ticketName!
        mTicketWeight.text = "Cân nặng: " + data.ticketWeight!
        mTicketSize.text = "Kích thước: " + data.ticketSize!
        mTicketPaymentMethod.text = "Phương thức thanh toán: " + data.ticketPaymentMethod!
        mTicketPickDate.text = "Ngày lấy hàng: " + data.ticketPickDate!
        mTicketCOD.text = "Tiền thu hộ: " + (data.ticketCOD! == "" ? "không thu hộ" : data.ticketCOD!)
        mTicketNote.text = "Ghi chú: " + (data.ticketNote! == "" ? "không có ghi chú" : data.ticketNote!)
        
        shipperID = data.shipperID
    }
    
    func onError() {
        Utils.showDialog(host: self, title: "Có Lỗi", message: "Chúng tôi không thể lấy được thông tin đơn hàng")
    }
}

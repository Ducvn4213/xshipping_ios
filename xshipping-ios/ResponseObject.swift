struct ResponseObject {
    var result : Bool?
    var data : String?
    
    init() {}
    
    init(result: Bool, data: String) {
        self.result = result
        self.data = data
    }
}

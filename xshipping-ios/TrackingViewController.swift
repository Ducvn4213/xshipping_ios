import UIKit
import GoogleMaps

protocol TrackingPositionCallback {
    func onSuccess(data: ResponsePosition)
    func onError()
}

class TrackingViewController : UIViewController, GMSMapViewDelegate {
    var mapView : GMSMapView!
    var london : GMSMarker?
    var londonView : UIImageView?
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    
    var gigID : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        mService?.getTicketPosition(id: gigID, callback: self)
    }
    
    override func loadView() {
        mService = appDelegate.getFSService()
        let camera = GMSCameraPosition.camera(withLatitude: 51.5, longitude: -0.127, zoom: 14)
        let mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        view = mapView
        
        mapView.delegate = self
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
    }
}

extension TrackingViewController : TrackingPositionCallback {
    func onSuccess(data: ResponsePosition) {
        let lat = data.lat
        let lon = data.lon
        let position = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
        let marker = GMSMarker(position: position)
        marker.title = "Shipper"
        marker.tracksViewChanges = true
        marker.map = mapView
        london = marker
        
        let camera = GMSCameraPosition.camera(withLatitude: lat!, longitude: lon!, zoom: 14)
        mapView.camera = camera
    }
    
    func onError() {
        let alert = UIAlertController(title: "Tìm Shipper", message: "Không thể lấy thông tin vị trí đơn hàng", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

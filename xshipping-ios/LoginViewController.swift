import UIKit
import FacebookLogin
import FacebookCore

protocol LoginCallback {
    func onSuccess(user: User)
    func onError(error: String)
}

struct MyProfileRequest: GraphRequestProtocol {
    struct Response: GraphResponseProtocol {
        var id : String?
        var name : String?
        var email : String?
        init(rawResponse: Any?) {
            let theResponse = rawResponse as! NSDictionary
            id = theResponse.object(forKey: "id") as? String
            name = theResponse.object(forKey: "name") as? String
            email = theResponse.object(forKey: "email") as? String
        }
    }
    
    var graphPath = "/me"
    var parameters: [String : Any]? = ["fields": "id, name, email"]
    var accessToken = AccessToken.current
    var httpMethod: GraphRequestHTTPMethod = .GET
    var apiVersion: GraphAPIVersion = .defaultVersion
}

class LoginViewController : UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    var hostNavController : UINavigationController!
    
    @IBOutlet weak var mLogin: UIButton!
    @IBOutlet weak var mLoginWithFace: UIButton!
    @IBOutlet weak var mLoginContainer: UIView!
    @IBOutlet weak var mLoadingContainer: UIView!
    @IBOutlet weak var mPassword: UITextField!
    @IBOutlet weak var mEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mService = appDelegate.getFSService()
        hostNavController = appDelegate.getHostNavController()
        
        configUI()
        configEvent()
    }
    
    func configEvent() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func configUI() {
        mLogin.layer.cornerRadius = 4
        mLogin.layer.masksToBounds = true
        
        mLoginWithFace.layer.cornerRadius = 4
        mLoginWithFace.layer.masksToBounds = true
        
        mLoginContainer.layer.cornerRadius = 4
        mLoginContainer.layer.masksToBounds = true
        
        mLoadingContainer.isHidden = true
        mLoadingContainer.backgroundColor = UIColor.black.withAlphaComponent(0.8)
    }
    
    @IBAction func onLogin(_ sender: Any) {
        if (!Utils.isValidEmail(str: mEmail.text!)) {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Email bạn nhập vào không đúng")
            return
        }
        
        if ((mPassword.text?.characters.count)! < 6) {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Mật khẩu bạn nhâp vào không đúng")
            return
        }
        
        mLoadingContainer.alpha = 0
        mLoadingContainer.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.mLoadingContainer.alpha = 1
        }, completion: {(Bool) in
            self.doLogin()
        })
    }
    
    func doLogin() {
        mService?.login(email: mEmail.text!, pass: mPassword.text!, callback: self)
    }
    
    @IBAction func doLoginFacebook(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn([ .publicProfile, .email], viewController: self, completion: {
            loginResult in
            switch loginResult {
            case .failed( _):
                Utils.showDialog(host: self.hostNavController, title: "Có Lỗi", message: "Có lỗi trong quá trình đăng nhập bằng facebook, vui lòng thử lại sau")
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, let accessToken):
                self.requestInfoFacebookAccount(accessToken: accessToken)
            }
        })
    }
    
    func requestInfoFacebookAccount(accessToken: AccessToken) {
        let connection = GraphRequestConnection()
        connection.add(MyProfileRequest()) { response, result in
            switch result {
            case .success(let response):
                let id = response.id
                let name = response.name
                let email = response.email
                
                self.mLoadingContainer.alpha = 0
                self.mLoadingContainer.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.mLoadingContainer.alpha = 1
                }, completion: {(Bool) in
                    self.mService?.loginFace(id: id!, name: name!, email: email!, callback: self)
                })
            case .failed(let error):
                print("Custom Graph Request Failed: \(error)")
            }
        }
        connection.start()
    }
}

extension LoginViewController : LoginCallback {
    func onSuccess(user: User) {
        mService?.setCurrentUser(user: user)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "sendgig")
        self.hostNavController.viewControllers = [someVC]
        
        Utils.showDialog(host: self.hostNavController, title: "Tìm Shipper", message: "Hi " + user.name! + ", chào mừng bạn quay trở lại với chúng tôi")
    }
    
    func onError(error: String) {
        Utils.showDialog(host: self, title: "Có Lỗi", message: "Email hoặc mật khẩu bạn nhập không đúng")
        mLoadingContainer.alpha = 0
        mLoadingContainer.isHidden = true
    }
}

import UIKit

protocol ChangePasswordCallback {
    func onSuccess()
    func onError()
}

class ChangePasswordViewController : UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    
    @IBOutlet weak var loadingContainer: UIView!
    @IBOutlet weak var mChangePasswordContainer: UIView!
    @IBOutlet weak var mOldPassword: UITextField!
    @IBOutlet weak var mNewPassword: UITextField!
    @IBOutlet weak var mReNewPassword: UITextField!
    @IBOutlet weak var mChangeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mService = appDelegate.getFSService()
        
        configUI()
    }
    
    func configUI() {
        mChangePasswordContainer.layer.cornerRadius = 4
        mChangePasswordContainer.layer.masksToBounds = true
        
        mChangeButton.layer.cornerRadius = 4
        mChangeButton.layer.masksToBounds = true
        
        loadingContainer.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        loadingContainer.isHidden = true
    }
    
    @IBAction func onChange(_ sender: Any) {
        if ((mOldPassword.text?.characters.count)! < 6) {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Mật khẩu cũ bạn nhập không đúng")
            return
        }
        
        if ((mNewPassword.text?.characters.count)! < 6) {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Mật khẩu mới bạn nhập không đúng")
            return
        }
        
        if (mReNewPassword.text != mNewPassword.text) {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Mật khẩu nhập lại không khớ")
            return
        }
        
        loadingContainer.alpha = 0
        loadingContainer.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.loadingContainer.alpha = 1
        })
        
        mService?.changePassword(old: mOldPassword.text!, new: mNewPassword.text!, callback: self)
    }
}

extension ChangePasswordViewController : ChangePasswordCallback {
    func onSuccess() {
        let alert = UIAlertController(title: "Tìm Shipper", message: "Thay đổi mật khẩu thành công", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func onError() {
        Utils.showDialog(host: self, title: "Có Lỗi", message: "Thay đổi mật khẩu không thành công. Vui lòng thử lại")
        loadingContainer.alpha = 0
        loadingContainer.isHidden = true
    }
}

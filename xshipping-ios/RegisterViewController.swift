import UIKit
import GooglePlaces
import CoreLocation

protocol RegisterCallback {
    func onSuccess(user: User)
    func onError(error: String)
}

class RegisterViewController : UIViewController {
    
    let manager = CLLocationManager()
    var placesClient: GMSPlacesClient!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?

    @IBOutlet weak var mRegisterContainer: UIView!
    @IBOutlet weak var mSubmit: UIButton!
    
    @IBOutlet weak var mEmail: UITextField!
    @IBOutlet weak var mName: UITextField!
    @IBOutlet weak var mAddress: UITextField!
    @IBOutlet weak var mPassword: UITextField!
    @IBOutlet weak var mRePassword: UITextField!
    @IBOutlet weak var loadingContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate.hostNavController = self.navigationController
        mService = appDelegate.getFSService()
        placesClient = GMSPlacesClient.shared()
        
        configUI()
        configEvent()
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            manager.requestAlwaysAuthorization()
        }
    }
    
    func configEvent() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func configUI() {
        mRegisterContainer.layer.cornerRadius = 4
        mRegisterContainer.layer.masksToBounds = true
        
        mSubmit.layer.cornerRadius = 4
        mSubmit.layer.masksToBounds = true
        
        loadingContainer.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        loadingContainer.alpha = 0
        loadingContainer.isHidden = true
    }
    @IBAction func requestCurrentLocation(_ sender: Any) {
        placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let placeLikelihoodList = placeLikelihoodList {
                let place = placeLikelihoodList.likelihoods.first?.place
                if let place = place {
                    //self.mAddress.text = place.name
                    self.mAddress.text = place.formattedAddress?.components(separatedBy: ", ")
                        .joined(separator: ", ")
                }
                else {
                    Utils.showDialog(host: self, title: "Có Lỗi", message: "Không thể lấy được vị trí hiện tại của bạn")
                }
            }
        })
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        if (mEmail.text == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Bạn chưa nhập email đăng ký")
            return
        }
        
        if (mName.text == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Bạn chưa nhập họ và tên đăng ký")
            return
        }
        
        if (mAddress.text == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Bạn chưa nhập địa chỉ đăng ký")
            return
        }
        
        if (mPassword.text == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Bạn chưa nhập mật khẩu đăng ký")
            return
        }
        
        if (mPassword.text != mRePassword.text) {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Mật khẩu bạn nhập lại không trùng khớp")
            return
        }
        
        loadingContainer.alpha = 0
        loadingContainer.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.loadingContainer.alpha = 1
        })
        
        mService?.register(email: mEmail.text!, name: mName.text!, address: mAddress.text!, pass: mPassword.text!, callback: self)
    }
}

extension RegisterViewController : RegisterCallback {
    func onSuccess(user: User) {
        mService?.setCurrentUser(user: user)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "verify")
        self.navigationController?.pushViewController(someVC, animated: true)
    }
    
    func onError(error: String) {
        Utils.showDialog(host: self, title: "Có Lỗi", message: "Đăng ký thất bại. Bạn vui lòng thử lại :(")
        loadingContainer.alpha = 0
        loadingContainer.isHidden = true
    }
}

import UIKit
import Alamofire

struct Param {
    var key: String?
    var value : String?
    
    init() {}
    
    init(key: String, value: String) {
        self.key = key
        self.value = value
    }
}

class Network : NSObject {
    override init() {
        super.init()
    }
    
    var onComplete: ((_ data: String)->())?
    var onError: ((_ error: String)->())?
    
    func execute(link: String, params: [Param]) {
        var paramList : [String : Any] = [:]
        for param in params {
            paramList.updateValue(param.value ?? "", forKey: param.key!)
        }
        Alamofire.request(link, method: .post, parameters: paramList).responseJSON { response in
            if let json = response.result.value {
                let jsonData = json as! [String: Any]
                let result = jsonData["result"] as! Bool
                let data = jsonData["data"] as! String
                
                if (result == true) {
                    self.onComplete!(data)
                }
                else {
                    self.onError!(data)
                }
            }
        }
    }
}

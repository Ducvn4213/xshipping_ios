
class Constants {
    static let HOST_API : String = "http://timshipper.top/api/api.php"
    static let HOST_GEO_DATA : String = "http://timshipper.top/api/getgeodata.php"
    static let HOST_IMAGE : String = "http://timshipper.top/api/"
    
    
    static let SHARE_PREFERENCE_USER_KEY : String = "SHARE_PREFERENCE_USER_KEY"
    
}

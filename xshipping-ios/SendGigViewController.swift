import UIKit

protocol LoadDistrictCallback {
    func onLoadDistrictSucess(data: [GeoData])
    func onLoadDistrictError(error: String)
}

protocol LoadWardCallback {
    func onLoadWardSucess(data: [GeoData])
    func onLoadWardError(error: String)
}

class SendGigViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mService : FSService?
    @IBOutlet weak var mName: UITextField!
    @IBOutlet weak var mPhone: UITextField!
    @IBOutlet weak var mAddress: UITextField!
    @IBOutlet weak var mCityButton: UIButton!
    @IBOutlet weak var mDistrictButton: UIButton!
    @IBOutlet weak var mWardButton: UIButton!
    
    fileprivate var mCity : String = ""
    fileprivate var mDistrict : String = ""
    fileprivate var mWard : String = ""
    fileprivate var isEditingMode: Bool = false
    fileprivate var allowEditWhenEditMode: Bool = false
    fileprivate var editingDistrict: String = ""
    fileprivate var editingWard: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate.hostNavController = self.navigationController
        mService = appDelegate.getFSService()
        
        configEvent()
        loadEditingData()
    }
    
    func loadEditingData() {
        let editTicketObject = mService?.getEditTicketObject()
        if (editTicketObject != nil) {
            isEditingMode = true
            mName.text = editTicketObject?.receiverName
            mPhone.text = editTicketObject?.receiverPhone
            mAddress.text = editTicketObject?.receiverJustAddress
            
            self.editingDistrict = (editTicketObject?.receiverDistrict)!
            self.editingWard = (editTicketObject?.receiverWard)!
            performChosseCity(city: (editTicketObject?.receiverProvince)!)
        }
    }
    
    func performChosseCity(city: String) {
        if (city == "Hồ Chí Minh") {
            self.mCityButton.setTitle("Hồ Chí Minh", for: .normal)
            if (self.mCity != "79") {
                self.mDistrict = ""
                self.mWard = ""
                self.mDistrictButton.setTitle("Bấm để chọn Quận/Huyện", for: .normal)
                self.mWardButton.setTitle("Bấm để chọn Phường/Xã", for: .normal)
            }
            self.mCity = "79"
        }
        else {
            self.mCityButton.setTitle("Bình Dương", for: .normal)
            if (self.mCity != "74") {
                self.mDistrict = ""
                self.mWard = ""
                self.mDistrictButton.setTitle("Bấm để chọn Quận/Huyện", for: .normal)
                self.mWardButton.setTitle("Bấm để chọn Phường/Xã", for: .normal)
            }
            self.mCity = "74"
        }
        
        mService?.loadDistricts(city_id: mCity, districtCallback: self)
    }

    func configEvent() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SendGigViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

    @IBAction func requestCityChanger(_ sender: Any) {
        let alert = UIAlertController(title: "Chọn Tỉnh/Thành Phố", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "Hồ Chí Minh", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.mCityButton.setTitle("Hồ Chí Minh", for: .normal)
            if (self.mCity != "79") {
                self.mDistrict = ""
                self.mWard = ""
                self.mDistrictButton.setTitle("Bấm để chọn Quận/Huyện", for: .normal)
                self.mWardButton.setTitle("Bấm để chọn Phường/Xã", for: .normal)
            }
            self.mCity = "79"
        }))
        alert.addAction(UIAlertAction(title: "Bình Dương", style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
            self.mCityButton.setTitle("Bình Dương", for: .normal)
            if (self.mCity != "74") {
                self.mDistrict = ""
                self.mWard = ""
                self.mDistrictButton.setTitle("Bấm để chọn Quận/Huyện", for: .normal)
                self.mWardButton.setTitle("Bấm để chọn Phường/Xã", for: .normal)
            }
            self.mCity = "74"
        }))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func requestDistrictChanger(_ sender: Any) {
        if (mCity == "") {
            Utils.showDialog(host: self, title: "Thông Báo", message: "Bạn chưa chọn Tỉnh/Thành Phố")
            return
        }
        mService?.loadDistricts(city_id: mCity, districtCallback: self)
    }
    
    @IBAction func requestWardChanger(_ sender: Any) {
        if (mDistrict == "") {
            Utils.showDialog(host: self, title: "Thông Báo", message: "Bạn chưa chọn Quận/Huyện")
            return
        }
        mService?.loadWards(district_id: mDistrict, wardCallback: self)
    }
    
    @IBAction func next(_ sender: Any) {
        if (mName.text == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Tên người nhận không thể thiếu")
            return
        }
        
        if (mPhone.text == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Số điện thoại người nhận không thể thiếu")
            return
        }
        
        if (mPhone.text?.characters.count != 10 && mPhone.text?.characters.count != 11) {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Số điện thoại người nhận không đúng")
            return
        }
        
        if (mAddress.text == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Địa chỉ người nhận không thể thiếu")
            return
        }
        
        let decimalCharacters = NSCharacterSet.decimalDigits
        
        let decimalRange = mAddress.text?.rangeOfCharacter(from: decimalCharacters, options: .numeric, range: nil)
        
        if decimalRange == nil {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Địa chỉ người nhận phải có cả chữ và số")
            return
        }
        
        if (mCity == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Thông tin thành phố không thể thiếu")
            return
        }
        
        if (mDistrict == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Thông tin Quận/Huyện không thể thiếu")
            return
        }
        
        if (mWard == "") {
            Utils.showDialog(host: self, title: "Có Lỗi", message: "Thông tin Phường/Xã không thể thiếu")
            return
        }
        
        mService?.setReceiver(name: mName.text, phone: mPhone.text, address: mAddress.text, city: mCityButton.title(for: .normal), districts: mDistrictButton.title(for: .normal), ward: mWardButton.title(for: .normal))
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let someVC = storyBoard.instantiateViewController(withIdentifier: "giginfo")
        self.navigationController?.pushViewController(someVC, animated: true)
    }
}

extension SendGigViewController : LoadDistrictCallback {
    func onLoadDistrictSucess(data: [GeoData]) {
        if (self.isEditingMode && !self.allowEditWhenEditMode) {
            for d in data {
                if (d.name == self.editingDistrict) {
                    self.mDistrictButton.setTitle(d.name, for: .normal)
                    self.mDistrict = d.id!
                    
                    self.mService?.loadWards(district_id: self.mDistrict, wardCallback: self)
                    return
                }
            }
            return
        }
        let alert = UIAlertController(title: "Chọn Quận/Huyện", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        for d in data {
            alert.addAction(UIAlertAction(title: d.name, style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
                self.mDistrictButton.setTitle(d.name, for: .normal)
                if (self.mDistrict != d.id) {
                    self.mWard = ""
                    self.mWardButton.setTitle("Bấm để chọn Phường/Xã", for: .normal)
                }
                self.mDistrict = d.id!
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func onLoadDistrictError(error: String) {
        Utils.showDialog(host: self, title: "Có Lỗi", message: error)
    }
}

extension SendGigViewController : LoadWardCallback {
    func onLoadWardSucess(data: [GeoData]) {
        if (self.isEditingMode && !self.allowEditWhenEditMode) {
            for d in data {
                if (d.name == self.editingWard) {
                    self.mWard = d.id!
                    self.mWardButton.setTitle(d.name, for: .normal)
                    self.allowEditWhenEditMode = true
                    return
                }
            }
            return
        }
        let alert = UIAlertController(title: "Chọn Phường/Xã", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        for d in data {
            alert.addAction(UIAlertAction(title: d.name, style: UIAlertActionStyle.default, handler: {(UIAlertAction) in
                self.mWardButton.setTitle(d.name, for: .normal)
                self.mWard = d.id!
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func onLoadWardError(error: String) {
        Utils.showDialog(host: self, title: "Có Lỗi", message: error)
    }
}

